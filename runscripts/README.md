Run-time helper scripts
=======================

This directory contains a couple of run-time helper scripts for the LINC pipeline.

* `extract_timing_info.py`
  Extract timing information from the log file produced by the LINC pipeline, when run through `cwltool`, for all the steps we consider relevant.

* `linc_create_jsonfile.sh`
  Generate an input JSON-file for processing the Measurement Sets in a given observation, specified by the Observation ID.

* `run_linc-cwltool.sh`
  A wrapper script to start the pipeline through `cwltool`. It sets up a number of default paths and filenames. Its input arguments are the Observation ID and the type of workflow to be used for processing. This script can be submitted as a batch job.

* `run_linc-toil-single.sh`
  A wrapper script to start the pipeline through `toil`, running on a single machine. It sets up a number of default paths and filenames. Its input arguments are the Observation ID and the type of workflow to be used for processing. This script can be submitted as a batch job.

* `run_linc-toil-slurm.sh`
  A wrapper script to start the pipeline through `toil`, using the Slurm batch system. It sets up a number of default paths and filenames. Its input arguments are the Observation ID and the type of workflow to be used for processing. This script should _not_ be submitted as a batch job, but run on the head node.
