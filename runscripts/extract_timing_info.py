#!/usr/bin/env python3

"""
Extract timing information from the log file produced by the LINC pipeline.
We are interested in lines that say that a certain step has started or completed
successfully. We then want to calculate the time difference between the start
and completion time for all the steps we consider relevant.

Log lines that we are interested in look like these:

    [2021-06-15 00:36:56] INFO [step h5parm_collector] start
    [2021-06-15 00:37:07] INFO [step h5parm_collector] completed success

So the time is enclosed between square brackets, as is the step name. We can
construct a regular expression that matches these lines, where we need to
parameterize on the name of the step, because not all the steps are large
enough to deserve extraction. Note that it is important to be quite strict in
the match pattern, because there are steps that we want to ignore, which have
names that are similar, but have trailing characters, like `h5parm_collector_2`.
"""
import sys
import re
from collections import namedtuple
from datetime import datetime

# Dict of tuples of steps for the workflow types that we want to collect timing
# information for.
WORKFLOW_STEPS = {
    "calibrator": ("prep", "pa", "fr", "bp", "ion"),
    "target": ("prep", "gsmcal", "finalize")
}

# Dict of possible states a step can be in. The key the string that appears in
# the log file, the value is a logical representation of that state.
STATES = {"start": "start", "completed success": "stop"}

# Named tuple that is used to store the group members of a matching regex.
Step = namedtuple("Step", ["time", "name", "state"])


# Exception classes
class DuplicateEntry(Exception):
    pass


class MissingEntry(Exception):
    pass


def strip_ansi(s):
    """
    Remove all ANSI escape sequences from string `s`. See Stack Overflow for
    details: https://stackoverflow.com/a/14693789
    :param s: input string which might contain ANSI escape sequences
    :return: string with ANSI escape sequences removed
    """
    pat = re.compile(r"\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])")
    return pat.sub("", s)


def process_file(filename, workflow_type):
    """
    Process file `filename`. Search for strings matching the log line pattern
    described above in the module description. For each matching line, create a
    namedtuple Step and add it to the return list.
    :param filename: Name of the log file to process.
    :param workflow_type: Must be "calibrator" or "target"
    :return: list of namedtuple Step.
    """
    # Create the regex pattern we're going to use to match log lines.
    time_pat = r"\[([\d: -]+)\]"
    level_pat = r"INFO"
    step_pat = r"\[step ({})\]".format("|".join(WORKFLOW_STEPS[workflow_type]))
    state_pat = r"({})".format("|".join(STATES.keys()))
    line_pat = rf"^{time_pat} {level_pat} {step_pat} {state_pat}$".format(
        time_pat, level_pat, step_pat, state_pat
    )
    pattern = re.compile(line_pat)

    matches = []
    with open(filename, "r") as f:
        for line in f.readlines():
            # Remove newline and ANSI characters
            line = strip_ansi(line.rstrip())
            # Match with the pattern
            match = pattern.match(line)
            if match:
                matches.append(Step._make(match.groups()))

    return matches


def timing_from_matches(matches):
    """
    Process the list of matching Steps contained in `matches`. Return a
    dictionary that contains the execution time for each step, which is defined
    as the time difference between the occurrence of the string 'start' and
    'completed success' for a given step.
    :param matches: list of namedtuple Step
    :return: dict with execution time (in seconds) for each step in `matches`
    """
    # Dictionary to hold information for each of the steps for which we want to
    # collect timing information. The key is the name of the step, the value
    # is a namedtuple Timing.
    start_stop_info = {}

    # First loop over all matches.
    for step in matches:
        item = start_stop_info.setdefault(step.name, {})
        if STATES[step.state] in item:
            raise DuplicateEntry(
                "ERROR: duplicate entry '{}' for step '{}'".format(
                    step.state, step.name
                )
            )
        else:
            item[STATES[step.state]] = datetime.strptime(step.time, "%Y-%m-%d %H:%M:%S")

    # Next calculate time differences between start and stop for each step.
    timing_info = {}
    for key, value in start_stop_info.items():
        try:
            timing_info[key] = int((value["stop"] - value["start"]).total_seconds())
        except KeyError as err:
            raise MissingEntry("ERROR: Missing state {} for step '{}'".format(err, key))

    return timing_info


def print_timing_info(timing_info):
    """
    Print timing information contained in the `timing_info` dict in a nicely
    formatted way.
    :param timing_info: dict with timing information
    :return:
    """
    separator = "+" + "-" * 31 + "+"
    colwidth = (len(separator) - 7) // 2
    layout = "| {:<%d} | {:>%d} |" % (colwidth, colwidth)
    print()
    print("Timing information".center(33))
    print(separator)
    print(layout.format("Step", "Run-time(s)"))
    print(separator)
    for key, value in timing_info.items():
        print(layout.format(key, value))
    print(separator)


def main(filename, workflow_type):
    """
    Extract the timing information from the given log file and print to stdout.
    :param filename: Name of the log file
    :param workflow_type: Must be "calibrator" or "target"
    :return: None
    """
    try:
        # Get a list of namedtuple Step, containing all matches
        matches = process_file(filename, workflow_type)

        # Get execution timing information from the list of steps.
        timing_info = timing_from_matches(matches)

        print_timing_info(timing_info)

    except Exception as err:
        print(err, file=sys.stderr)
        return 1

    return 0

def usage():
    print("Usage: extract_timing_info.py <log-file> calibrator|target", file=sys.stderr)
    sys.exit(1)


if __name__ == "__main__":
    try:
        log_file, workflow = sys.argv[1:]
    except ValueError:
        usage()
    if workflow not in WORKFLOW_STEPS:
        usage()
    sys.exit(main(log_file, workflow))
