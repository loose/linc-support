#!/bin/bash -eu
#
# Script to run the LINC pipeline on observation data.

# Error function
error()
{
  echo -e "ERROR: $@" >&2
  exit 1
}

# Check input arguments
[ $# -eq 2 ] || error "\
Usage: ${0} <observation-id> <work-flow>
    where <observation-id> is the ID of a given observation,
    and <work-flow> is the name of the workflow.
    Measurement Sets for the given Observation ID are searched for in the
    directory '${LINC_DATA_DIR}'.
    The currently supported workflows are: HBA_calibrator, and HBA_target."

OBSID=${1}
INPUT_DIR=${LINC_DATA_DIR}/${OBSID}
OUTPUT_DIR=${LINC_WORKING_DIR}/${OBSID}

WORKFLOW=${2}
WORKFLOW_DIR=${LINC_INSTALL_DIR}/share/linc/workflows
CWLFILE=${WORKFLOW_DIR}/${WORKFLOW}.cwl

JOBSTORE=${OUTPUT_DIR}/jobstore
LOGFILE=${OUTPUT_DIR}/${WORKFLOW}.log
LOGS_DIR=${OUTPUT_DIR}/logs
STATS_DIR=${OUTPUT_DIR}/stats
TMP_OUTPUT_DIR=${OUTPUT_DIR}/tmp
TMP_OUTDIR_PREFIX=${TMP_OUTPUT_DIR}/tmp.

# Check if there's a user-defined JSON-file in ${OUTPUT_DIR}. 
# If not, use the default JSON-file in ${LINC_DATA_DIR}.
JSONFILE=${OUTPUT_DIR}.json
[ -f ${JSONFILE} ] || JSONFILE=${INPUT_DIR}.json

# Increase open file limit to hardware limit
ulimit -n $(ulimit -Hn)

# Print all SLURM variables
echo -e "
================  SLURM variables  ================
$(for s in ${!SLURM@}; do echo "${s}=${!s}"; done)
===================================================
"

# Show current shell ulimits
echo -e "
============  Current resource limits  ============
$(ulimit -a)
===================================================
"

# Tell user what variables will be used
echo -e "
The LINC pipeline will run, using the following settings:
  Input directory            : ${INPUT_DIR}
  Input specification file   : ${JSONFILE}
  Workflow definition file   : ${CWLFILE}
  Output directory           : ${OUTPUT_DIR}
  Job store directory        : ${JOBSTORE}
  Failed jobs log directory  : ${LOGS_DIR}
  Job statistics directory   : ${STATS_DIR}
  Temporary output directory : ${TMP_OUTPUT_DIR}
"

# Check if directories and files actually exist. If not, bail out
[ -d ${INPUT_DIR} ] || error "Directory '${INPUT_DIR}' does not exist"
[ -f ${CWLFILE} ] || error "Workflow file '${CWLFILE}' does not exist"
[ -f ${JSONFILE} ] || error "Input specification file '${JSONFILE}' does not exist"

# Assume we want to restart the pipeline if job store exists
[ -d ${JOBSTORE} ] && RESTART="--restart" || RESTART=""

# Ensure output directories exists
mkdir -p ${LOGS_DIR}
mkdir -p ${STATS_DIR}
mkdir -p ${TMP_OUTPUT_DIR}

# Command that will be used to run the CWL workflow and its environment
COMMAND="toil-cwl-runner \
  --batchSystem slurm \
  --bypass-file-store \
  --coalesceStatusCalls \
  --disableCaching \
  --no-container \
  --preserve-entire-environment \
  --stats \
  --jobStore ${JOBSTORE}/ \
  --logFile ${LOGFILE} \
  --outdir ${OUTPUT_DIR}/ \
  --tmp-outdir-prefix ${TMP_OUTDIR_PREFIX} \
  --writeLogs ${LOGS_DIR}/ \
  ${RESTART} \
  ${CWLFILE} \
  ${JSONFILE}"
ENVIRON="TOIL_SLURM_ARGS=--export=ALL"

# Execute command (catch exit status like this, because of stop-on-error)
echo "env ${ENVIRON} ${COMMAND}"
env ${ENVIRON} ${COMMAND} > ${OUTPUT_DIR}/${WORKFLOW}.out && STATUS=${?} || STATUS=${?}

# Save job statistics
echo -e "\nSaving job statistics. Please wait ...\n"
toil stats --raw ${JOBSTORE} > ${STATS_DIR}/${WORKFLOW}.stats.json || true
toil stats --pretty ${JOBSTORE} > ${STATS_DIR}/${WORKFLOW}.stats.txt || true

# Report success of failure
if [ ${STATUS} -eq 0 ]
then
  echo -e "\n\nSUCCESS: Pipeline finished successfully\n\n"
else
  echo -e "\n\n**FAILURE**: Pipeline failed with exit status: ${STATUS}\n\n"
fi
exit ${STATUS}
