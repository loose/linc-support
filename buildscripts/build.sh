#!/bin/bash -e
#
# Build script for LINC
#
# NOTE: The LINC pipeline uses the same software stack as the Rapthor pipeline.
#       This build script therefore assumes that you have already installed the
#       Rapthor pipeline software stack.

script="$(cd $(dirname $0) && pwd)/$(basename $0)"

# GOTO for bash, based upon https://stackoverflow.com/a/31269848/5353461
# Ref: https://stackoverflow.com/a/52872489
goto()
{
 local label=$1
 cmd=$(sed -En "/^[[:space:]]*#[[:space:]]*$label:[[:space:]]*#/{:a;n;p;ba};" "$script")
 /bin/echo -e "\n[WARNING] ====> Jumping to label $label <====\n" >&2
 eval "$cmd"
 exit
}


#############################################################################
#                        P  R  E  A  M  B  L  E                             #
#############################################################################

/bin/echo -e "\n\n================ Pre-install  ===============\n\n"

# Avoid issues with non-ascii characters 
export LC_ALL=C.UTF-8

PROJECT_ROOT=/project/rapthor
PACKAGE=linc

DEFAULT_BUILD_DIR=/tmp/${USER}/${PACKAGE}-build
DEFAULT_DATA_DIR=${PROJECT_ROOT}/Data/${PACKAGE}
DEFAULT_INSTALL_DIR=${PROJECT_ROOT}/Software/${PACKAGE}
DEFAULT_WORKING_DIR=${PROJECT_ROOT}/Share/${PACKAGE}

BUILD_DIR=${BUILD_DIR:-${DEFAULT_BUILD_DIR}}
DATA_DIR=${DATA_DIR:-${DEFAULT_DATA_DIR}}
INSTALL_DIR=${INSTALL_DIR:-${DEFAULT_INSTALL_DIR}}
SCRIPT_DIR=$(cd $(dirname $0) && pwd)
WORKING_DIR=${WORKING_DIR:-${DEFAULT_WORKING_DIR}}

/bin/echo "BUILD_DIR   : ${BUILD_DIR}"
/bin/echo "INSTALL_DIR : ${INSTALL_DIR}"

PYTHON=${PYTHON:-/usr/bin/python3}
PIP=${PIP:-${PYTHON} -m pip}

INST_BIN_DIR=${INSTALL_DIR}/bin
INST_DATA_DIR=${INSTALL_DIR}/share/${PACKAGE}
INST_INC_DIR=${INSTALL_DIR}/include
INST_LIB_DIR=${INSTALL_DIR}/lib
INST_SBIN_DIR=${INSTALL_DIR}/sbin

INST_PY_DIR=$(${PYTHON} -c \
  "from distutils.sysconfig import get_python_lib; \
   print(get_python_lib(prefix='${INSTALL_DIR}'))")

export PYTHONPATH=${INST_PY_DIR}:${PYTHONPATH}
export PYTHONUSERBASE=${INSTALL_DIR}

export LINC_DATA_DIR=${DATA_DIR}
export LINC_INSTALL_DIR=${INSTALL_DIR}
export LINC_WORKING_DIR=${WORKING_DIR}
export LINC_DATA_ROOT=${INST_DATA_DIR}

# Create build and install directories
mkdir -p ${BUILD_DIR}
mkdir -p ${INST_BIN_DIR}
mkdir -p ${INST_DATA_DIR}
mkdir -p ${INST_LIB_DIR}
mkdir -p ${INST_PY_DIR}
mkdir -p ${INST_SBIN_DIR}


#############################################################################
#            P y t h o n   s e t u p t o o l s   p a c k a g e s            #
#############################################################################

# LINC: #
#-- Name: linc
NAME=linc
BRANCH=master
/bin/echo -e "\n\n================ Building ${NAME} ================\n\n"
cd ${BUILD_DIR}
if [ ! -d ${NAME}/.git ]; then
  git clone https://git.astron.nl/RD/LINC.git ${NAME}
fi
cd ${NAME}
git fetch
git checkout ${BRANCH}
git pull
PYTHONUSERBASE=${INSTALL_DIR} ${PIP} install --upgrade --user .


#############################################################################
#                         P  O  S  T  A  M  B  L  E                         #
#############################################################################

/bin/echo -e "\n\n================ Post-install  ===============\n\n"
cd ${SCRIPT_DIR}

# Copy helper scripts to sbin directory
cp -r ${SCRIPT_DIR}/../runscripts/* ${INST_SBIN_DIR}

# Generate file that can be sourced to set environment variables
echo "Generating rc-file ..."
mkdir -p ${INSTALL_DIR}/etc
cat > ${INSTALL_DIR}/etc/linc.rc <<EOF
export PATH=${INST_SBIN_DIR}:${INST_BIN_DIR}:\${PATH}
export PYTHONPATH=${INST_BIN_DIR}:${INST_PY_DIR}:\${PYTHONPATH}
$(for p in ${!LINC_@}; do echo "export ${p}=${!p}"; done)
EOF

# Generate file with one-liner package version descriptions
echo "Generating versions-file ..."
cat > ${INSTALL_DIR}/sw-versions.txt <<EOF
$(for p in $(find $BUILD_DIR -type d -name .git | sort)
  do 
    d=$(dirname $p); b=$(basename $d); cd $d; l=$(git log -1 --oneline)
    echo "$b: $l"
  done
)
EOF

/bin/echo -e "\n\n================ ALL DONE! ================\n\n"
