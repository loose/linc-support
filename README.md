Support stuff for LINC
======================

LINC (LOFAR Initial Calibration) is a reimplementation in CWL of the Prefactor pipeline.

This repository contains stuff written to support building/running the LINC pipeline on different platforms. The LINC repository can be found at https://git.astron.nl/RD/LINC.git.

Note that LINC makes use of external software components, like DP3 and WSClean, the same software stack that is used by the Rapthor pipeline. 

Stuff to support building/running the Rapthor pipeline can be found at https://git.astron.nl/loose/rapthor-support.git.
