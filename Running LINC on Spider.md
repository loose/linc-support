# Running LINC on Spider

LINC can be run in a number of different ways on Spider. Here, I will briefly show how.

For people who don't have full access to the Rapthor project area on Spider, I've put one calibrator input data set (`L667520`) and the helper script `linc_create_jsonfile.sh` in the directory `/project/rapthor/Public/linc`. 

If you want to use that directory, then you should make sure that you use

```bash
LINC_DATA_DIR=/project/rapthor/Public/linc/data
```

in the following examples.

### **Important note**

*Please execute the following commands on one of the compute nodes, instead of the head node. This will be much appreciated by the system administrators.*



## Running LINC from *within* Singularity

* Download the latest docker image from Docker Hub and convert it to a singularity image on the fly:
  ```bash
  singularity pull docker://astronrd/linc
  ```
  
* Start the container, mount the required directories:

  ```bash
  LINC_INSTALL_DIR=/usr/local \
  LINC_DATA_DIR=/project/rapthor/Data/linc \
  LINC_WORKING_DIR=/project/rapthor/Share/linc \
  bash -c '\
    singularity shell \
      -e \
      --env LINC_INSTALL_DIR=${LINC_INSTALL_DIR} \
      --env LINC_DATA_DIR=${LINC_DATA_DIR} \
      --env LINC_WORKING_DIR=${LINC_WORKING_DIR} \
      --bind ${LINC_DATA_DIR}:${LINC_DATA_DIR}:ro \
      --bind ${LINC_WORKING_DIR}:${LINC_WORKING_DIR}:rw \
      --writable-tmpfs \
      linc_latest.sif \
  '
  ```
  
* Inside the container:

  * Create an input JSON-file for observation `L667520` (if one does not exist yet):
    ```bash
    cd ${LINC_WORKING_DIR}
    ./linc_create_jsonfile.sh ${LINC_DATA_DIR}/L667520
    ```
    
  * Start the pipeline either using `cwltool` or `toil`.

### Using CWLTool

  * Still inside the container, start the pipeline using the following command:
    ```bash
    OBSID=L667520
    USER=${USER:-$(id -un)}
    cwltool \
      --no-container \
      --parallel \
      --preserve-entire-environment \
      --timestamps \
      --outdir ${LINC_WORKING_DIR}/${OBSID}/ \
      --tmpdir-prefix /tmp/${USER}/${OBSID}/  \
      ${LINC_INSTALL_DIR}/share/linc/workflows/HBA_calibrator.cwl \
      ${LINC_WORKING_DIR}/${OBSID}.json
    ```

### Using Toil

- Still inside the container, start the pipeline using the following command:
  ```bash
  OBSID=L667520
  USER=${USER:-$(id -un)}
  ulimit -n $(ulimit -Hn)
  mkdir -p ${LINC_WORKING_DIR}/${OBSID}
  mkdir -p /tmp/${USER}/${OBSID}/tmp
  toil-cwl-runner \
    --batchSystem single_machine \
    --bypass-file-store \
    --disableProgress \
    --no-container \
    --preserve-entire-environment \
    --stats \
    --jobStore /tmp/${USER}/${OBSID}/jobstore/ \
    --logFile ${LINC_WORKING_DIR}/${OBSID}/HBA_calibrator.log \
    --outdir ${LINC_WORKING_DIR}/${OBSID}/ \
    --tmp-outdir-prefix /tmp/${USER}/${OBSID}/tmp/tmp. \
    --writeLogs ${LINC_WORKING_DIR}/${OBSID}/logs/ \
    ${LINC_INSTALL_DIR}/share/linc/workflows/HBA_calibrator.cwl \
    ${LINC_WORKING_DIR}/${OBSID}.json
  ```
  
  **Note: the pipeline can stall for hours with the current version of `toil`. [This merge request](https://github.com/common-workflow-language/cwltool/pull/1949) in the `cwltool` package resolves the issue.**



## Running LINC *outside* a container

There are two ways to run LINC outside a container. 

1. Run LINC *completely* outside a container. 
   In this case, a lot of extra software needs to be compiled and installed on the host system. This way of running LINC is therefore not recommended, unless you are a developer and want to have full control over the software stack being used.
2. Run LINC outside the container, but run each of the CWL steps *inside* a container.
   In this case, the CWL runner will ensure that, if the CWL steps has a `DockerRequirement` in its `hints` section, the step is executed inside the container mentioned in the `dockerPull` statement in that section.

In the following, we will focus on the second way, but in both cases you will need to fetch the source tree first.

* Fetch the `LINC` sources:
  ```bash
  git clone https://git.astron.nl/RD/LINC.git
  ```

* Next, create a Python virtual environment, activate it, and install `toil`, `cwltool`, and `LINC` itself.
  ```bash
  cd LINC
  python3.9 -m venv venv
  . venv/bin/activate
  pip install -U pip
  pip install toil[cwl]
  pip install .
  ```

* Set some environment variables:
  ```bash
  export LINC_INSTALL_DIR=$VIRTUAL_ENV
  export LINC_DATA_DIR=/project/rapthor/Data/linc
  export LINC_DATA_ROOT=$LINC_INSTALL_DIR/share/linc
  export LINC_WORKING_DIR=/project/rapthor/Share/linc
  export OBSID=L667520
  ```
  
* Create an input JSON-file for observation `L667520`:

  ```bash
  cd ${LINC_WORKING_DIR}
  ./linc_create_jsonfile.sh ${LINC_DATA_DIR}/${OBSID}
  ```


**NOTE: It is strongly recommended to pre-fetch the Docker image `astronrd/linc`, to convert it to a Singularity image and store that image on local storage, e.g. `/tmp`. If the singularity image is stored on shared storage, the pipeline will run extremely slow!**

### Using CWLTool

Start the pipeline using the following command:

```bash
cwltool \
  --parallel \
  --singularity \
  --timestamps \
  --outdir ${LINC_WORKING_DIR}/${OBSID}/ \
  --tmpdir-prefix /tmp/${USER}/${OBSID}/  \
  ${LINC_INSTALL_DIR}/share/linc/workflows/HBA_calibrator.cwl \
  ${LINC_WORKING_DIR}/${OBSID}.json
```

### Using Toil

When using `toil`, it is possible to run LINC on multiple nodes, using [Slurm](https://slurm.schedmd.com/). In this case, the pipeline should preferably be started on the head node, instead of being run as a Slurm job, to avoid that the main process gets killed when it exceeds its maximum run-time. You also need to supply the command-line argument `--batchSystem slurm`, and need to ensure that the intermediate pipeline results are stored on a shared disk. The complete command then becomes:

```bash
mkdir -p ${LINC_WORKING_DIR}/${OBSID}/tmp
TOIL_SLURM_ARGS=--export=ALL \
toil-cwl-runner \
  --batchSystem slurm \
  --bypass-file-store \
  --coalesceStatusCalls \
  --disableCaching \
  --preserve-entire-environment \
  --singularity \
  --stats \
  --jobStore ${LINC_WORKING_DIR}/${OBSID}/jobstore/ \
  --outdir ${LINC_WORKING_DIR}/${OBSID}/ \
  --tmp-outdir-prefix ${LINC_WORKING_DIR}/${OBSID}/tmp/tmp. \
  --writeLogs ${LINC_WORKING_DIR}/${OBSID}/logs/ \
  ${LINC_INSTALL_DIR}/share/linc/workflows/HBA_calibrator.cwl \
  ${LINC_WORKING_DIR}/${OBSID}.json
```

**Note: the pipeline can stall for hours with the current version of `toil`. [This merge request](https://github.com/common-workflow-language/cwltool/pull/1949) in the `cwltool` package resolves the issue.**
