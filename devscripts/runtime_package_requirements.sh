#!/bin/bash
#
# Derive which Debian packages are required at run-time by user-compiled
# binaries.
#
# This script searches for ELF binary files in the given prefix (default:
# /usr/local), using the `file` utility, and determines which libraries
# are needed by each file, using `readelf`. Each library is searched for
# in sys_prefix (default /usr). When found, `dpkg` is used to find the
# package containing the library.

prefix=${prefix:-/usr/local}
sys_prefix=${sys_prefix:-/usr}

# Find all binary ELF files in the prefix 
elf_files=$(\
    find ${prefix} -type f | \
    xargs file | \
    grep -E ':[[:blank:]]+ELF' | \
    cut -d: -f1 \
)

# Find all libraries needed by the ELF binaries
needed=$(\
    readelf -d ${elf_files} | \
    sed -rn '/NEEDED/s/^.*\[(.*)\]/\1/p' | \
    sort -u \
)

# Compose a match pattern for egrep, by escaping any specials characters
# in ${needed} and by replacing spaces with '$|', thus adding an EOL marker
# and a regex OR character after each library name.
pattern=$(echo ${needed} | \
    sed -e "s/\([][}{)(?^$.*+\\]\)/\\\\\1/g" -e "s/ /$|/g" -e "s/$/$/"
)

# Search all needed libraries in the ${sys_prefix}, ignoring ${prefix}.
# Dereference any symbolic links, and remove duplicates.
libraries=$(\
    find ${sys_prefix} -not -path "${prefix}/*" | \
    egrep ${pattern} | \
    xargs realpath | \
    sort -u
)

# Find the Debian packages containing the libraries (remove duplicates). 
# Ensure to dereference any symbolic links before querying. 
for lib in ${libraries}
do
    dpkg -S ${lib} 2>/dev/null
done | cut -d: -f1 | sort -u
