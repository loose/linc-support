#!/bin/sh
#
# Watch the total number of open files used by the current user

trap 'STATUS=$?; echo "Max. number of open file descriptors: $max_nf"; exit $STATUS' 0 1 2 3 15

max_nf=0
while true
do
  nf=$(
    for d in $(find /proc -maxdepth 1 -user $USER 2>/dev/null)
    do
      # Count number of file descriptors, ignoring the current process
      [ "$d" != "/proc/$$" ] && ls -1 $d/fd 2>/dev/null
    done | wc -l
  )
  echo "$(date +%H:%M:%S.%N): $nf"
  if [ $nf -gt $max_nf ]
  then
    max_nf=$nf
    echo "============================================================="
    for d in $(find /proc -maxdepth 1 -user $USER 2>/dev/null)
    do
      [ "$d" = "/proc/$$" ] && continue
      [ -d "$d" ] && echo "** $d **" && ls -l $d/exe && ls -l $d/fd
    done
    echo "============================================================="
  fi
  sleep 0.5
done

