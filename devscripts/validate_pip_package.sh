#!/bin/bash -e
#
# Validate a pip-installable python package 

# Describe usage
usage()
{
    cat >&2 <<EOF

Usage: ${0} <python-package> [path/spec/url]

    where <python-package> is the name of a pip-installable package.
    The optional [url/path/spec] is used to locate the package, where
    [path] is a local file path, [spec] a requirements specifier, and
    [url] an archive url.

EOF
}


# Cleanup function
cleanup()
{
    [ -n "${TMPDIR}" ] && rm -rf "${TMPDIR}"
}


# Trap to cleanup
trap 'STATUS=${?}; cleanup; exit ${STATUS}' 0 1 2 3 15


# Check input arguments
[ ${#} -ge 1 -a ${#} -le 2 ] || { usage; exit 1; }


# Initialize variables
PACKAGE=${1}
PATH_SPEC_URL=${2:-${PACKAGE}}
TMPDIR=$(mktemp -d)


# Create virtual environment and activate it
python3 -m venv ${TMPDIR}
. ${TMPDIR}/bin/activate


# Update pip
python3 -m pip install --upgrade pip


# Install package to validate
python3 -m pip install ${PATH_SPEC_URL}


# Check module imports 
cat <<EOF

==========================  Checking module imports  ===========================
EOF
for f in $(python3 -m pip show -f ${PACKAGE} \
            | grep -v '/__pycache__/' | grep -v '/bin/' | grep '\.py$' \
            | sed -e 's,\.py$,,' -e 's,/,.,g')
do
    printf "%-70s  " "${f}"
    err=$(python3 -c "import ${f}" |& egrep '(ModuleNotFound|Import)Error') \
        && printf "%8s\n\t%s\n" "[FAIL]" "${err}" \
        || printf "%8s\n" "[ OK ]"
done 


# Check executable scripts
cat <<EOF

========================  Checking executable scripts  =========================
EOF
for f in $(python3 -m pip show -f ${PACKAGE} \
            | grep -v '/__pycache__/' | grep '/bin/')
do
    f=$(basename ${f})
    printf "%-70s  " "${f}"
    err=$(${f} |& egrep '(ModuleNotFound|Import)Error') \
        && printf "%8s\n\t%s\n" "[FAIL]" "${err}" \
        || printf "%8s\n" "[ OK ]"
done

# We're done
cat <<EOF

Done.
EOF
