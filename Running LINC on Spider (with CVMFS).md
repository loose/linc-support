# Running LINC on Spider (with CVMFS)

LINC can be run in a number of different ways on Spider. Here, I will briefly show how.

## Preparation

For people who don't have full access to the Rapthor project area on Spider, I've put one calibrator input data set (`L667520`) and the helper script `linc_create_jsonfile.sh` in the directory `/project/rapthor/Public/linc`. 

If you want to use that directory, you need to set up the following.

* Point to the public data directory in the examples below:

  ```bash
  LINC_DATA_DIR=/project/rapthor/Public/linc/data
  ```

* Create and use a working directory you can write to:

  ```bash
  LINC_WORKING_DIR=/home/${USER}/linc_test
  mkdir -p ${LINC_WORKING_DIR}
  ```
  
* Copy the helper script to create an input JSON-file from the public directory to your working directory:
  ```bash
  cp /project/rapthor/Public/linc/linc_create_jsonfile.sh ${LINC_WORKING_DIR}
  ```

### **Important note**

*Please execute the following commands on one of the compute nodes, instead of the head node. This will be much appreciated by the system administrators.*



## Running LINC from *within* Singularity

* Start an interactive session on Spider (or submit the command below as a Slurm job):
  
  ```bash
  srun --partition=interactive -c 8 --pty bash -i -l
  ```

* Start the container, mount the required directories:

  ```bash
  LINC_INSTALL_DIR=/usr/local \
  LINC_DATA_DIR=/project/rapthor/Public/linc/data \
  LINC_WORKING_DIR=/home/${USER}/linc_test \
  bash -c '\
    singularity shell \
      -e \
      --env LINC_INSTALL_DIR=${LINC_INSTALL_DIR} \
      --env LINC_DATA_DIR=${LINC_DATA_DIR} \
      --env LINC_WORKING_DIR=${LINC_WORKING_DIR} \
      --bind ${LINC_DATA_DIR}:${LINC_DATA_DIR}:ro \
      --bind ${LINC_WORKING_DIR}:${LINC_WORKING_DIR}:rw \
      --writable-tmpfs \
      /cvmfs/softdrive.nl/surfadvisors-tkok/ASTRON/LINC/astronrd_linc.sif \
  '
  ```
  
* Inside the container:

  * Create an input JSON-file for observation `L667520` (if one does not exist yet):
    ```bash
    cd ${LINC_WORKING_DIR}
    ./linc_create_jsonfile.sh ${LINC_DATA_DIR}/L667520
    ```
    
  * Start the pipeline either using `cwltool` or `toil`.

### Using CWLTool

  * Still inside the container, start the pipeline using the following command:
    ```bash
    OBSID=L667520
    USER=${USER:-$(id -un)}
    cwltool \
      --no-container \
      --parallel \
      --preserve-entire-environment \
      --timestamps \
      --outdir ${LINC_WORKING_DIR}/${OBSID}/ \
      --tmpdir-prefix /tmp/${USER}/${OBSID}/  \
      ${LINC_INSTALL_DIR}/share/linc/workflows/HBA_calibrator.cwl \
      ${LINC_WORKING_DIR}/${OBSID}.json
    ```

### Using Toil

- Still inside the container, start the pipeline using the following command:
  ```bash
  OBSID=L667520
  USER=${USER:-$(id -un)}
  ulimit -n $(ulimit -Hn)
  mkdir -p ${LINC_WORKING_DIR}/${OBSID}
  mkdir -p /tmp/${USER}/${OBSID}/tmp
  toil-cwl-runner \
    --batchSystem single_machine \
    --bypass-file-store \
    --disableProgress \
    --no-container \
    --preserve-entire-environment \
    --stats \
    --jobStore /tmp/${USER}/${OBSID}/jobstore/ \
    --logFile ${LINC_WORKING_DIR}/${OBSID}/HBA_calibrator.log \
    --outdir ${LINC_WORKING_DIR}/${OBSID}/ \
    --tmp-outdir-prefix /tmp/${USER}/${OBSID}/tmp/tmp. \
    --writeLogs ${LINC_WORKING_DIR}/${OBSID}/logs/ \
    ${LINC_INSTALL_DIR}/share/linc/workflows/HBA_calibrator.cwl \
    ${LINC_WORKING_DIR}/${OBSID}.json
  ```
  
  **Note: the pipeline can stall for hours with the current version of `toil`. [This merge request](https://github.com/common-workflow-language/cwltool/pull/1949) in the `cwltool` package resolves the issue.**

### Output
You can find the results in the working directory:

```bash
ls ${LINC_WORKING_DIR}/L667520
```

## Running LINC *outside* a container

There are two ways to run LINC outside a container. 

1. Run LINC *completely* outside a container. 
   In this case, a lot of extra software needs to be compiled and installed on the host system. This way of running LINC is therefore not recommended, unless you are a developer and want to have full control over the software stack being used.
2. Run LINC outside the container, but run each of the CWL steps *inside* a container.
   In this case, the CWL runner will ensure that, if the CWL steps has a `DockerRequirement` in its `hints` section, the step is executed inside the container mentioned in the `dockerPull` statement in that section. We will use images already present on spider via CVMFS.

In the following, we will focus on the second way, but in both cases you will need to fetch the source tree first.

* Fetch the `LINC` sources:
  ```bash
  git clone https://git.astron.nl/RD/LINC.git
  ```

* Next, create a Python virtual environment, activate it, and install `toil`, `cwltool`, and `LINC` itself.
  ```bash
  cd LINC
  python3.9 -m venv venv
  . venv/bin/activate
  pip install -U pip
  pip install toil[cwl]
  pip install .
  ```

* Set some environment variables:
  ```bash
  export LINC_INSTALL_DIR=$VIRTUAL_ENV
  export LINC_DATA_DIR=/project/rapthor/Public/linc/data 
  export LINC_DATA_ROOT=$LINC_INSTALL_DIR/share/linc
  export LINC_WORKING_DIR=/home/$USER/linc_test
  export CWL_SINGULARITY_CACHE=/cvmfs/softdrive.nl/surfadvisors-tkok/ASTRON/LINC
  export OBSID=L667520
  ```
  Setting `CWL_SINGULARITY_CACHE` ensures that CWLTool or Toil will try to fetch a singularity image from the cache first, thus avoiding a download if there's a cache hit.
  
* Create an input JSON-file for observation `L667520`:

  ```bash
  cd ${LINC_WORKING_DIR}
  ./linc_create_jsonfile.sh ${LINC_DATA_DIR}/${OBSID}
  ```

### Using CWLTool

* Start an interactive session on Spider (or submit the below as a Slurm job):
  
  ```bash
  srun --partition=interactive -c 8 --pty bash -i -l
  ```

- Start the pipeline using the following command:
  ```bash
  cwltool \
    --parallel \
    --singularity \
    --timestamps \
    --outdir ${LINC_WORKING_DIR}/${OBSID}/ \
    --tmpdir-prefix /tmp/${USER}/${OBSID}/  \
    ${LINC_INSTALL_DIR}/share/linc/workflows/HBA_calibrator.cwl \
    ${LINC_WORKING_DIR}/${OBSID}.json
  ```

### Using Toil

When using `toil`, it is possible to run LINC on multiple nodes, using [Slurm](https://slurm.schedmd.com/). In this case, the pipeline should preferably be started on the head node, instead of being run as a Slurm job, to avoid that the main process gets killed when it exceeds its maximum run-time. You also need to supply the command-line argument `--batchSystem slurm`, and need to ensure that the intermediate pipeline results are stored on a shared disk. 

The complete command then becomes:

```bash
mkdir -p ${LINC_WORKING_DIR}/${OBSID}/tmp
TOIL_SLURM_ARGS=--export=ALL \
toil-cwl-runner \
  --batchSystem slurm \
  --bypass-file-store \
  --coalesceStatusCalls \
  --disableCaching \
  --preserve-entire-environment \
  --singularity \
  --stats \
  --jobStore ${LINC_WORKING_DIR}/${OBSID}/jobstore/ \
  --outdir ${LINC_WORKING_DIR}/${OBSID}/ \
  --tmp-outdir-prefix ${LINC_WORKING_DIR}/${OBSID}/tmp/tmp. \
  --writeLogs ${LINC_WORKING_DIR}/${OBSID}/logs/ \
  ${LINC_INSTALL_DIR}/share/linc/workflows/HBA_calibrator.cwl \
  ${LINC_WORKING_DIR}/${OBSID}.json
```

**Note: the pipeline can stall for hours with the current version of `toil`. [This merge request](https://github.com/common-workflow-language/cwltool/pull/1949) in the `cwltool` package resolves the issue.**

### Output
You can find the results in the working directory:

```bash
ls ${LINC_WORKING_DIR}/L667520
```

